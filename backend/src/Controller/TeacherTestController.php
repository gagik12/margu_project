<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class TeacherTestController extends AbstractController
{
    /**
     * @Route("/teacher/test", name="teacher_test")
     */
    public function index()
    {
        return $this->render('teacher_test/index.html.twig', [
            'controller_name' => 'TeacherTestController',
        ]);
    }
}
