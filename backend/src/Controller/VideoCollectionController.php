<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class VideoCollectionController extends AbstractController
{
    /**
     * @Route("/video/collection", name="video_collection")
     */
    public function index()
    {
        return $this->render('video_collection/index.html.twig', [
            'controller_name' => 'VideoCollectionController',
        ]);
    }
}
