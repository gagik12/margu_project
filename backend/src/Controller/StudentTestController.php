<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class StudentTestController extends AbstractController
{
    /**
     * @Route("/student/test", name="student_test")
     */
    public function index()
    {
        return $this->render('student_test/index.html.twig', [
            'controller_name' => 'StudentTestController',
        ]);
    }
}
